// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, onAuthStateChanged, signOut, sendPasswordResetEmail } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-auth.js";


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyA9cwwMFTEqcGSgOGI1TTc70p0zsm-m6U4",
    authDomain: "econome-8f48c.firebaseapp.com",
    databaseURL: "https://econome-8f48c-default-rtdb.firebaseio.com",
    projectId: "econome-8f48c",
    storageBucket: "econome-8f48c.appspot.com",
    messagingSenderId: "529502500069",
    appId: "1:529502500069:web:d3778cacbd0ed03c04900c"
  };
  
  // Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth();

// Función autoinvocada para el formulario de inicio de sesión
(function() {
    const loginForm = document.getElementById("loginForm");
    if (loginForm) {
        loginForm.addEventListener("submit", function(e) {
            e.preventDefault();
            var userEmail = document.getElementById("userEmail").value;
            var userPassword = document.getElementById("userPassword").value;

            signInWithEmailAndPassword(auth, userEmail, userPassword)
                .then((userCredential) => {
                    // Usuario ha iniciado sesión correctamente
                    var user = userCredential.user;
                    alert("Inicio de sesión exitoso!");
                    window.location.href ='/html/dashboard.html';
                })
                .catch((error) => {
                    // Error al iniciar sesión
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    alert("Error: " + errorMessage);
                });
        });
    }
})();